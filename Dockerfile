FROM openjdk:8-alpine

RUN mkdir /apps

COPY ./build/libs/spring-cloud-config-server.jar /apps/spring-cloud-config-server.jar

HEALTHCHECK --interval=10s --start-period=2m CMD curl -f http://localhost:8080/actuator/health | exit 1

CMD java -jar /apps/spring-cloud-config-server.jar

EXPOSE 8080
